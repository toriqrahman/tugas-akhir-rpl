<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <section class="ftco-section">
    <div class="container">
    <form action="<?php echo site_url('shop/checkout/order'); ?>" method="POST">

      <div class="row justify-content-center">
        <div class="col-xl-7 ftco-animate">
                <h3 class="mb-4 billing-heading">Alamat Pengiriman</h3>

                

                <div class="form-group">
                    <label for="note" class="form-control-label">Tulis alamat lengkap dan no hp anda</label>
                    <textarea name="note" class="form-control" id="note"></textarea>
                </div>

        </div>
        <div class="col-xl-5">
            <div class="row mt-5 pt-3">
                <div class="col-md-12 d-flex mb-5">
                    <div class="cart-detail cart-total p-3 p-md-4">
                        <h3 class="billing-heading mb-4">Rincian Belanja</h3>
                        <p class="d-flex">
                                  <span>Subtotal</span>
                                  <span>Rp <?php echo format_rupiah($subtotal); ?></span>
                              </p>

                              <p class="d-flex total-price">
                                  <span>Total</span>
                                  <span>Rp <?php echo format_rupiah($total); ?></span>
                              </p>
                              </div>
                </div>
                <div class="col-md-12">
                    <div class="cart-detail p-3 p-md-4">
                        <h3 class="billing-heading mb-4">Metode Order</h3>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                          <div class="radio">
                                             <label><input type="radio" name="payment" class="mr-2" value="1"> Delivery</label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-md-12">
                                          <div class="radio">
                                             <label><input type="radio" name="payment" class="mr-2" value="2" checked> Take Away</label>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="form-group text-right" style="margin-top: 10px;">
                <input type="submit" class="btn btn-primary py-2 px-2" value="Buat Pesanan">
            </div>
                </div>

                
            </div>
        </div> <!-- .col-md-8 -->
      </div>

    </form>
    </div>
  </section> <!-- .section -->